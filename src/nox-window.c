#include <libpanel.h>

#include "nox-window.h"

struct _NoxWindow
{
  GtkApplicationWindow parent_instance;
  PanelDock *dock;
  PanelGrid *grid;
  GMenuModel *page_menu;
  GtkDropDown *language;
  GtkToggleButton *frame_header_bar;
  GtkMenuButton *primary_button;
  AdwSplitButton *run_button;
  PanelThemeSelector *theme_selector;
  GtkLabel *command;
  GtkLabel *command_bar;
};

G_DEFINE_TYPE (NoxWindow, nox_window, ADW_TYPE_APPLICATION_WINDOW)

static GdkRGBA white;
static GdkRGBA grey;
static GdkRGBA black;

GtkWidget *
nox_window_new (GtkApplication *application)
{
  return g_object_new (NOX_TYPE_WINDOW,
                       "application", application,
                       NULL);
}

static gboolean
on_save_cb (PanelSaveDelegate *delegate,
            GTask *task,
            PanelWidget *widget)
{
  g_assert (PANEL_IS_SAVE_DELEGATE (delegate));
  g_assert (G_IS_TASK (task));
  g_assert (PANEL_IS_WIDGET (widget));

  // actually do the save here, ideally asynchronously

  g_print ("Actually save the file\n");

  panel_widget_set_modified (widget, FALSE);
  panel_save_delegate_set_progress (delegate, 1.0);
  g_task_return_boolean (task, TRUE);

  return TRUE;
}

static gboolean
text_to_visible (GBinding *binding,
                 const GValue *from_value,
                 GValue *to_value,
                 gpointer user_data)
{
  const char *str = g_value_get_string (from_value);
  g_value_set_boolean (to_value, str && *str);
  return TRUE;
}

static void
nox_window_add_document (NoxWindow *self)
{
  g_return_if_fail (NOX_IS_WINDOW (self));
}

static void
add_document_action (GtkWidget *widget,
                     const char *action_name,
                     GVariant *param)
{
  nox_window_add_document (NOX_WINDOW (widget));
}

static void
project_build_action (GtkWidget *widget,
                      const char *action_name,
                      GVariant *param)
{
}

static void
set_theme_action (GSimpleAction *action,
                  GVariant *param,
                  gpointer user_data)
{
  const char *str = g_variant_get_string (param, NULL);
  AdwStyleManager *manager = adw_style_manager_get_default ();

  if (g_strcmp0 (str, "default") == 0)
    adw_style_manager_set_color_scheme (manager, ADW_COLOR_SCHEME_DEFAULT);
  else if (g_strcmp0 (str, "light") == 0)
    adw_style_manager_set_color_scheme (manager, ADW_COLOR_SCHEME_FORCE_LIGHT);
  else if (g_strcmp0 (str, "dark") == 0)
    adw_style_manager_set_color_scheme (manager, ADW_COLOR_SCHEME_FORCE_DARK);
}

static void
set_runner_action (GSimpleAction *action,
                   GVariant *param,
                   gpointer user_data)
{
  g_simple_action_set_state (action, param);
}

static void
set_high_contrast_action (GSimpleAction *action,
                          GVariant *param,
                          gpointer user_data)
{
  g_autoptr (GVariant) v = g_action_get_state (G_ACTION (action));

  if (!v ||
      !g_variant_is_of_type (v, G_VARIANT_TYPE_BOOLEAN) ||
      g_variant_get_boolean (v) == FALSE)
    g_simple_action_set_state (action, g_variant_new_boolean (TRUE));
  else
    g_simple_action_set_state (action, g_variant_new_boolean (FALSE));
}

static void
set_rtl_action (GSimpleAction *action,
                GVariant *param,
                gpointer user_data)
{
  g_autoptr (GVariant) v = g_action_get_state (G_ACTION (action));

  if (!v ||
      !g_variant_is_of_type (v, G_VARIANT_TYPE_BOOLEAN) ||
      g_variant_get_boolean (v) == FALSE)
    g_simple_action_set_state (action, g_variant_new_boolean (TRUE));
  else
    g_simple_action_set_state (action, g_variant_new_boolean (FALSE));
}

static void
notify_theme_cb (NoxWindow *self,
                 GParamSpec *pspec,
                 AdwStyleManager *style_manager)
{
  const char *name;
  GAction *action;

  g_assert (NOX_IS_WINDOW (self));
  g_assert (ADW_IS_STYLE_MANAGER (style_manager));

  switch (adw_style_manager_get_color_scheme (style_manager))
    {
    case ADW_COLOR_SCHEME_PREFER_DARK:
    case ADW_COLOR_SCHEME_FORCE_DARK:
      name = "dark";
      break;

    case ADW_COLOR_SCHEME_FORCE_LIGHT:
    case ADW_COLOR_SCHEME_PREFER_LIGHT:
      name = "light";
      break;

    case ADW_COLOR_SCHEME_DEFAULT:
    default:
      if (!adw_style_manager_get_system_supports_color_schemes (style_manager))
        name = "light";
      else
        name = "default";
      break;
    }

  action = g_action_map_lookup_action (G_ACTION_MAP (self), "theme");
  g_simple_action_set_state (G_SIMPLE_ACTION (action),
                             g_variant_new_string (name));
}

static PanelFrame *
create_frame_cb (PanelGrid *grid,
                 NoxWindow *self)
{
  PanelFrame *frame;
  PanelFrameHeader *header;
  AdwStatusPage *status;
  GtkGrid *shortcuts;
  GtkWidget *child;

  g_assert (NOX_IS_WINDOW (self));

  frame = PANEL_FRAME (panel_frame_new ());

  status = ADW_STATUS_PAGE (adw_status_page_new ());
  adw_status_page_set_title (status, "Open a File or Terminal");
  adw_status_page_set_icon_name (status, "document-new-symbolic");
  adw_status_page_set_description (status, "Use the page switcher above or use one of the following:");
  shortcuts = GTK_GRID (gtk_grid_new ());
  gtk_grid_set_row_spacing (shortcuts, 6);
  gtk_grid_set_column_spacing (shortcuts, 32);
  gtk_widget_set_halign (GTK_WIDGET (shortcuts), GTK_ALIGN_CENTER);
  gtk_grid_attach (shortcuts, gtk_label_new ("New Document"), 0, 0, 1, 1);
  gtk_grid_attach (shortcuts, gtk_label_new ("Ctrl+N"), 1, 0, 1, 1);
  gtk_grid_attach (shortcuts, gtk_label_new ("Close Document"), 0, 1, 1, 1);
  gtk_grid_attach (shortcuts, gtk_label_new ("Ctrl+W"), 1, 1, 1, 1);
  for (child = gtk_widget_get_first_child (GTK_WIDGET (shortcuts));
       child;
       child = gtk_widget_get_next_sibling (child))
    gtk_widget_set_halign (child, GTK_ALIGN_START);
  adw_status_page_set_child (status, GTK_WIDGET (shortcuts));
  panel_frame_set_placeholder (frame, GTK_WIDGET (status));

  if (gtk_toggle_button_get_active (self->frame_header_bar))
    header = PANEL_FRAME_HEADER (panel_frame_header_bar_new ());
  else
    header = PANEL_FRAME_HEADER (panel_frame_tab_bar_new ());

  if (PANEL_IS_FRAME_HEADER_BAR (header))
    panel_frame_header_bar_set_show_icon (PANEL_FRAME_HEADER_BAR (header), TRUE);

  panel_frame_set_header (frame, header);
  panel_frame_header_add_prefix (header,
                                 -100,
                                 (child = g_object_new (GTK_TYPE_BUTTON,
                                                        "width-request", 40,
                                                        "focus-on-click", FALSE,
                                                        "icon-name", "go-previous-symbolic",
                                                        NULL)));
  gtk_widget_add_css_class (child, "flat");

  panel_frame_header_add_prefix (header,
                                 -50,
                                 (child = g_object_new (GTK_TYPE_BUTTON,
                                                        "width-request", 40,
                                                        "focus-on-click", FALSE,
                                                        "icon-name", "go-next-symbolic",
                                                        NULL)));
  gtk_widget_add_css_class (child, "flat");

  return frame;
}

static void
nox_window_constructed (GObject *object)
{
  NoxWindow *self = (NoxWindow *) object;

  G_OBJECT_CLASS (nox_window_parent_class)->constructed (object);

  /* Create 0,0 frame */
  (void) panel_grid_column_get_row (panel_grid_get_column (self->grid, 0), 0);
}

static void
nox_window_class_init (NoxWindowClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

  object_class->constructed = nox_window_constructed;

  gtk_widget_class_set_template_from_resource (widget_class, "/org/feliwir/nox/nox-window.ui");
  gtk_widget_class_bind_template_child (widget_class, NoxWindow, dock);
  gtk_widget_class_bind_template_child (widget_class, NoxWindow, grid);
  gtk_widget_class_bind_template_child (widget_class, NoxWindow, page_menu);
  gtk_widget_class_bind_template_child (widget_class, NoxWindow, frame_header_bar);
  gtk_widget_class_bind_template_child (widget_class, NoxWindow, language);
  gtk_widget_class_bind_template_child (widget_class, NoxWindow, command);
  gtk_widget_class_bind_template_child (widget_class, NoxWindow, command_bar);
  gtk_widget_class_bind_template_child (widget_class, NoxWindow, primary_button);
  gtk_widget_class_bind_template_child (widget_class, NoxWindow, run_button);
  gtk_widget_class_bind_template_child (widget_class, NoxWindow, theme_selector);
  gtk_widget_class_bind_template_callback (widget_class, create_frame_cb);

  gtk_widget_class_install_action (widget_class, "document.new", NULL, add_document_action);
  gtk_widget_class_install_action (widget_class, "project.build", NULL, project_build_action);

  gtk_widget_class_add_binding_action (widget_class, GDK_KEY_n, GDK_CONTROL_MASK, "document.new", NULL);
  gtk_widget_class_add_binding_action (widget_class, GDK_KEY_F9, 0, "win.reveal-start", NULL);
  gtk_widget_class_add_binding_action (widget_class, GDK_KEY_F9, GDK_CONTROL_MASK, "win.reveal-bottom", NULL);
  gtk_widget_class_add_binding_action (widget_class, GDK_KEY_F9, GDK_SHIFT_MASK, "win.reveal-end", NULL);

  gdk_rgba_parse (&white, "#fff");
  gdk_rgba_parse (&grey, "#1e1e1e");
  gdk_rgba_parse (&black, "#000");
}

static void
nox_window_init (NoxWindow *self)
{
  static const GActionEntry entries[] = {
    { "theme", NULL, "s", "'default'", set_theme_action },
    { "runner", NULL, "s", "''", set_runner_action },
    { "high-contrast", set_high_contrast_action, NULL, "false" },
    { "right-to-left", set_rtl_action, NULL, "false" },
  };
  g_autoptr (GPropertyAction) reveal_start = NULL;
  g_autoptr (GPropertyAction) reveal_end = NULL;
  g_autoptr (GPropertyAction) reveal_bottom = NULL;
  AdwStyleManager *style_manager = adw_style_manager_get_default ();
  GtkPopover *popover;

  gtk_widget_init_template (GTK_WIDGET (self));

  g_action_map_add_action_entries (G_ACTION_MAP (self), entries, G_N_ELEMENTS (entries), self);
  g_signal_connect_object (style_manager,
                           "notify",
                           G_CALLBACK (notify_theme_cb),
                           self,
                           G_CONNECT_SWAPPED);
  notify_theme_cb (self, NULL, style_manager);

  reveal_start = g_property_action_new ("reveal-start", self->dock, "reveal-start");
  reveal_bottom = g_property_action_new ("reveal-bottom", self->dock, "reveal-bottom");
  reveal_end = g_property_action_new ("reveal-end", self->dock, "reveal-end");

  g_action_map_add_action (G_ACTION_MAP (self), G_ACTION (reveal_start));
  g_action_map_add_action (G_ACTION_MAP (self), G_ACTION (reveal_end));
  g_action_map_add_action (G_ACTION_MAP (self), G_ACTION (reveal_bottom));

  for (GtkWidget *child = gtk_widget_get_first_child (GTK_WIDGET (self->language));
       child;
       child = gtk_widget_get_next_sibling (child))
    {
      /* Override to force upwards */
      if (GTK_IS_POPOVER (child))
        gtk_popover_set_position (GTK_POPOVER (child), GTK_POS_TOP);
    }

  popover = gtk_menu_button_get_popover (self->primary_button);
  gtk_popover_menu_add_child (GTK_POPOVER_MENU (popover),
                              GTK_WIDGET (self->theme_selector),
                              "theme");
}
