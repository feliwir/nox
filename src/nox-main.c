#include <adwaita.h>
#include <libpanel.h>

#include "nox-window.h"

static void
on_activate_cb(GtkApplication *app)
{
    GtkWidget *window = nox_window_new(app);
    gtk_window_present(GTK_WINDOW(window));
}

static void
on_startup_cb(GtkApplication *app)
{
    panel_init();
    gtk_icon_theme_add_resource_path(gtk_icon_theme_get_for_display(gdk_display_get_default()), "/icons");
}

int main(int argc,
         char *argv[])
{
    GApplication *app;
    int ret;

    app = g_object_new(ADW_TYPE_APPLICATION,
                       "application-id", "org.gnome.libpanel.example",
                       NULL);
    g_signal_connect(app,
                     "startup",
                     G_CALLBACK(on_startup_cb),
                     NULL);
    g_signal_connect(app,
                     "activate",
                     G_CALLBACK(on_activate_cb),
                     NULL);

    ret = g_application_run(app, argc, argv);

    g_object_unref(app);

    return ret;
}
