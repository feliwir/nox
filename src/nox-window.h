#pragma once

#include <adwaita.h>

G_BEGIN_DECLS

#define NOX_TYPE_WINDOW (nox_window_get_type())

G_DECLARE_FINAL_TYPE(NoxWindow, nox_window, NOX, WINDOW, AdwApplicationWindow)

GtkWidget *nox_window_new(GtkApplication *application);

G_END_DECLS
